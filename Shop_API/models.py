from django.db import models

# USER MODEL
class Users(models.Model):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    user_ID     = models.AutoField(primary_key=True)
    fullName    = models.CharField(max_length=20)
    email       = models.EmailField(max_length=50)
    phone       = models.IntegerField()
    address     = models.CharField(max_length=100)
    gender      = models.CharField(max_length=10, choices=GENDER_CHOICES)
    password    = models.CharField(max_length=20)
    
    is_User     = models.BooleanField(default=False)
    is_Vendor   = models.BooleanField(default=False)
    is_Staff    = models.BooleanField(default=False)
    is_RD       = models.BooleanField(default=False)

    status      = models.BooleanField(default=False)

    def __str__(self):
        return self.fullName

# VENDORS MODEL 
class Vendors(models.Model):
    vendor_ID           = models.AutoField(primary_key=True)
    user_ID             = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)

    ownerName           = models.CharField(max_length=50)
    shopName            = models.CharField(max_length=100)
    shopNumber          = models.CharField(max_length=50)
    shopAddress         = models.CharField(max_length=100)
    NID_Photo           =  models.ImageField(upload_to='images/Vendors/', default='', blank=True)

    registrationNumber  = models.CharField(max_length=100)
    tradeLicense        = models.ImageField(upload_to='images/Vendors/', default='', blank=True)
    refferenceName      = models.CharField(max_length=50)
    refferenceRelation  = models.CharField(max_length=100)
    refferencePhone     = models.CharField(max_length=100)

    status              = models.BooleanField(default=False)

    def __str__(self):
        return self.ownerName

# RD MODEL 
class RD(models.Model):
    rd_ID               = models.AutoField(primary_key=True)
    user_ID             = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    rdImage             = models.ImageField(upload_to='images/RD/', default='', blank=True)

    rdApplication       = models.ImageField(upload_to='images/RD/', default='', blank=True)
    rdExperienceDoc     = models.ImageField(upload_to='images/RD/', default='', blank=True)
    rdAgreement         = models.ImageField(upload_to='images/RD/', default='', blank=True)
    
    NID_No              = models.IntegerField()
    rd_NID_Photo        = models.ImageField(upload_to='images/RD/', default='', blank=True)
    businessExperience  = models.CharField(max_length=100)

    refferenceName      = models.CharField(max_length=50)
    refferenceRelation  = models.CharField(max_length=100)
    refferenceAddress   = models.CharField(max_length=100)
    refferencePhone     = models.CharField(max_length=20)

    status              = models.BooleanField(default=False)

    def __str__(self):
        return self.refferenceName

# WISHLIST MODEL
class Wishlist(models.Model):
    wishlist_ID     = models.AutoField(primary_key=True)
    user_ID         = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    
    wishlistName    = models.CharField(max_length=50)
    
    def __str__(self):
        return self.wishlistName

# PRODUCT SUB-CATEGORY 
class Sub_Category(models.Model):
    subCategory_ID      = models.AutoField(primary_key=True)
    
    subCategoryName     = models.CharField(max_length=50)
    subCategoryslug     = models.CharField(max_length=40)

    def __str__(self):
        return self.subCategoryName

# PRODUCT CATEGORY MODEL
class ProductCategory(models.Model):
    category_ID     = models.AutoField(primary_key=True)
    subCategory_ID  = models.ForeignKey( Sub_Category, default=None, on_delete= models.CASCADE)
    
    categoryName    = models.CharField(max_length=50)

    def __str__(self):
        return self.categoryName

# PRODUCT COMMENTS MODEL
class Comments(models.Model):
    comment_ID  = models.AutoField(primary_key=True)
    user_ID     = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    
    comment     = models.CharField(max_length=200)
    status      = models.BooleanField(default=False)

    def __str__(self):
        return self.comment

# REPORT ABOUT PRODUCTS- MODEL 
class Report(models.Model):
    report_ID       = models.AutoField(primary_key=True)
    user_ID         = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    
    reporter        = models.CharField(max_length=50)
    reportTitle     = models.CharField(max_length=40)
    product         = models.CharField(max_length=50)
    datetime        = models.DateTimeField(auto_now_add=True, blank= True)
    status          = models.BooleanField(default=False)

    def __str__(self):
        return self.reportTitle

# PRODUCTS MODEL
class Products(models.Model):
    product_ID              = models.AutoField(primary_key=True)

    Vendor_ID               = models.ForeignKey( Vendors, default=None, on_delete= models.CASCADE)
    comment_ID              = models.ForeignKey( Comments, default=None, on_delete= models.CASCADE)
    category_ID             = models.ForeignKey( ProductCategory, default=None, on_delete= models.CASCADE)

    # Product image will be a array type which will take multiple images
    productImage            = models.ImageField(upload_to='images/Products/', default='', blank=True)

    productName_EN          = models.CharField(max_length=30)
    productDescription_EN   = models.TextField()
    productName_BN          = models.CharField(max_length=30)
    productDescription_BN   = models.TextField()

    size                    = models.CharField(max_length=50)
    color                   = models.CharField(max_length=50)
    sku                     = models.CharField(max_length=50)
    price                   = models.CharField(max_length=50)
    oldPrice                = models.CharField(max_length=50)
    productType             = models.CharField(max_length=50)
    slug                    = models.CharField(max_length=50)
    quantity                = models.CharField(max_length=50)
    weight                  = models.CharField(max_length=50)
    datetime                = models.DateTimeField(auto_now_add=True, blank= True)

    status                  = models.BooleanField(default=False)

    def __str__(self):
        return self.productName_EN

# DELIVERY PARTNER MODEL 
class DeliveryPartner(models.Model):
    dp_ID       = models.AutoField(primary_key=True)

    dpName      = models.CharField(max_length=50) 
    dpPhone     = models.CharField(max_length=20) 
    dpAddress   = models.CharField(max_length=100) 
    dpEmail     = models.EmailField(max_length=50) 

    longitude   = models.DecimalField(max_digits=9, decimal_places=6) 
    latitude    = models.DecimalField(max_digits=9, decimal_places=6) 

    status      = models.BooleanField(default=False)

    def __str__(self):
        return self.dpName

# PAYMENT MODEL 
class Payment(models.Model):
    payment_ID          = models.AutoField(primary_key=True)
    user_ID             = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)

    paymentType         = models.CharField(max_length=100) 
    code                = models.CharField(max_length=50) 
    paymentMethod       = models.CharField(max_length=50) 
    paymentValidation   = models.CharField(max_length=50) 
    paymentExpireDate   = models.DateField(auto_now_add=True, blank= True)

    status              = models.BooleanField(default=False)

    def __str__(self):
        return self.payment_ID


# ORDER MODEL 
class Order(models.Model):
    order_ID        = models.AutoField(primary_key=True)
    user_ID         = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    payment_ID      = models.ForeignKey( Payment, default=None, on_delete= models.CASCADE)
    dp_ID           = models.ForeignKey( DeliveryPartner, default=None, on_delete= models.CASCADE)

    orderNumber     = models.IntegerField()
    orderDateTime   = models.DateTimeField(auto_now_add=True, blank= True)
   
    city            = models.CharField(max_length=50) 
    province        = models.CharField(max_length=50)
    orderTime       = models.TimeField(auto_now=True, blank=True)
    deliveryTime    = models.TimeField(auto_now=True, blank=True)
    totalPrice      = models.CharField(max_length=50)
    coupoun         = models.CharField(max_length=50)
    discount        = models.CharField(max_length=50)
    shipping        = models.CharField(max_length=50)

    status          = models.BooleanField(default=False)

    def __str__(self):
        return self.orderNumber

# ORDER DETAILS MODEL 
class OrderDetail(models.Model):
    orderDetail_ID  = models.AutoField(primary_key=True)
    order_ID        = models.ForeignKey( Order, default=None, on_delete= models.CASCADE)
    product_ID      = models.ForeignKey( Products, default=None, on_delete= models.CASCADE)

    quantity        = models.CharField(max_length=50) 
    totalPrice      = models.CharField(max_length=50)
    discount        = models.CharField(max_length=50)

    status          = models.BooleanField(default=False)

    def __str__(self):
        return self.orderDetail_ID

# CART MODEL 
class Cart(models.Model):
    cart_ID         = models.AutoField(primary_key=True)
    user_ID         = models.ForeignKey( Users, default=None, on_delete= models.CASCADE)
    product_ID      = models.ForeignKey( Products, default=None, on_delete= models.CASCADE)

    quantity        = models.CharField(max_length=50) 
    totalPrice      = models.CharField(max_length=50)
    city            = models.CharField(max_length=50) 
    province        = models.CharField(max_length=50)
    dateTime        = models.DateTimeField(auto_now=True, blank=True)

    status          = models.BooleanField(default=False)

    def __str__(self):
        return self.cart_ID

# INVITE FRIENDS MODEL 
class InviteFriend(models.Model):
    friend_ID   = models.AutoField(primary_key=True)
    user_ID     = models.ForeignKey(Users, default=None, on_delete=models.CASCADE)
    friendName  = models.CharField(max_length=50)

    def __str__(self):
        return self.friendName
