from django.shortcuts import render
from rest_framework.response import Response

from Shop_API.models import *
from Shop_API.serializers import *

from rest_framework import permissions
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework import filters
from rest_framework.filters import SearchFilter

from rest_framework.decorators import api_view
from rest_framework.reverse import reverse


# USERS MODEL CRUD OPERATION THROUGH API 
class CreateUsers(generics.ListCreateAPIView):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer
    # permission_classes = [IsAdminUser]

class UsersDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer

class UsersList(generics.ListAPIView):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['fullName']

# VENDORS MODEL CRUD OPERATION THROUGH API 
class CreateVendors(generics.ListCreateAPIView):
    queryset = Vendors.objects.all()
    serializer_class = VendorsSerializer

class VendorsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vendors.objects.all()
    serializer_class = VendorsSerializer

class VendorsList(generics.ListAPIView):
    queryset = Vendors.objects.all()
    serializer_class = VendorsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['shopName']

# RD MODEL CRUD OPERATION THROUGH API 
class CreateRD(generics.ListCreateAPIView):
    queryset = RD.objects.all()
    serializer_class = RDSerializer

class RDDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = RD.objects.all()
    serializer_class = RDSerializer

class RDList(generics.ListAPIView):
    queryset = RD.objects.all()
    serializer_class = RDSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['fullName']

# WISHLIST MODEL CRUD OPERATION THROUGH API 
class CreateWishlist(generics.ListCreateAPIView):
    queryset = Wishlist.objects.all()
    serializer_class = WishlistSerializer

class WishlistDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Wishlist.objects.all()
    serializer_class = WishlistSerializer

class Wishlist_List(generics.ListAPIView):
    queryset = Wishlist.objects.all()
    serializer_class = WishlistSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['wishlistName']

# SUB-CATEGORY MODEL CRUD OPERATION THROUGH API 
class CreateSub_Category(generics.ListCreateAPIView):
    queryset = Sub_Category.objects.all()
    serializer_class = Sub_CategorySerializer

class Sub_CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sub_Category.objects.all()
    serializer_class = Sub_CategorySerializer

class Sub_CategoryList(generics.ListAPIView):
    queryset = Sub_Category.objects.all()
    serializer_class = Sub_CategorySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['subCategoryName']

# CATEGORY MODEL CRUD OPERATION THROUGH API 
class CreateProductCategory(generics.ListCreateAPIView):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer

class ProductCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer

class ProductCategoryList(generics.ListAPIView):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['categoryName']

# COMMENTS MODEL CRUD OPERATION THROUGH API
# COMMENTS CAN BE DESTROY OR DELETE 
class CreateComments(generics.ListCreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer

class CommentsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer

# REPORT MODEL CRUD OPERATION THROUGH API
# REPORT CAN BE DESTROY OR DELETE 
class CreateReport(generics.ListCreateAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

class ReportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

# PRODUCTS MODEL CRUD OPERATION THROUGH API 
class CreateProducts(generics.ListCreateAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer

class ProductsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer

class ProductsList(generics.ListAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['productName_EN']

# DELIVERY PARTNER MODEL CRUD OPERATION THROUGH API 
class CreateDeliveryPartner(generics.ListCreateAPIView):
    queryset = DeliveryPartner.objects.all()
    serializer_class = DeliveryPartnerSerializer

class DeliveryPartnerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DeliveryPartner.objects.all()
    serializer_class = DeliveryPartnerSerializer

class DeliveryPartnerList(generics.ListAPIView):
    queryset = DeliveryPartner.objects.all()
    serializer_class = DeliveryPartnerSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['dpName']

# PAYMENT MODEL CRUD OPERATION THROUGH API 
class CreatePayment(generics.ListCreateAPIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

class PaymentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

class PaymentList(generics.ListAPIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['paymentMethod','code']

# ORDER MODEL CRUD OPERATION THROUGH API 
class CreateOrder(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class OrdersDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class OrderList(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['orderNumber','orderDateTime']

# ORDER DETAILS MODEL CRUD OPERATION THROUGH API 
class CreateOrderDetail(generics.ListCreateAPIView):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer

class OrderDetail_Detail(generics.RetrieveUpdateDestroyAPIView):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer

class OrderDetailList(generics.ListAPIView):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['orderDetail_ID']

# CART MODEL CRUD OPERATION THROUGH API 
class CreateCart(generics.ListCreateAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

class CartDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

class CartList(generics.ListAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['cart_ID']

# INVITE FRIENDS CRUD OPERATION THROUGH API 
class CreateInviteFriend(generics.ListCreateAPIView):
    queryset = InviteFriend.objects.all()
    serializer_class = InviteFriendSerializer

class InviteFriendDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InviteFriend.objects.all()
    serializer_class = InviteFriendSerializer

class InviteFriendList(generics.ListAPIView):
    queryset = InviteFriend.objects.all()
    serializer_class = InviteFriendSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['friendName']

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'createUser': reverse('createUser', request=request, format=format),
        'usersList': reverse('usersList', request=request, format=format),
        
        'createVendor': reverse('createVendor', request=request, format=format),
        'productList': reverse('productList', request=request, format=format),
        
        'createProduct': reverse('createProduct', request=request, format=format),
        'productList': reverse('productList', request=request, format=format),
        
        'createOrder': reverse('createOrder', request=request, format=format),
        'orderList': reverse('orderList', request=request, format=format),
    })