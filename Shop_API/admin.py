from django.contrib import admin
from Shop_API.models import Users,Vendors,RD,Wishlist,Sub_Category,ProductCategory,Comments,Report,Products,DeliveryPartner,Payment,Order,OrderDetail,Cart,InviteFriend
# Register your models here.

admin.site.register((
    Users,
    Vendors,
    RD,
    Wishlist,
    Sub_Category,
    ProductCategory,
    Comments,
    Report,
    Products,
    DeliveryPartner,
    Payment,
    Order,
    OrderDetail,
    Cart,
    InviteFriend,
    ))