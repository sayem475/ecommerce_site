from django.urls import path,include
from Shop_API.views import *
# from rest_framework import routers
# router = routers.DefaultRouter()
# router.register(r'UsersDetail', UsersViewSet)
from Shop_API import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
  
    path('', views.api_root),

    path('createUser/', CreateUsers.as_view(), name="createUser"),
    path('usersList/',UsersList.as_view(), name="usersList"),
    path('userDetails/<int:pk>/', UsersDetail.as_view(), name="userDetails"),

    path('createVendor/', CreateVendors.as_view(), name="createVendor"),
    path('vendorsList/',VendorsList.as_view(), name="vendorsList"),
    path('vendorDetails/<int:pk>/', UsersDetail.as_view(), name="vendorDetails"),

    path('createProduct/', CreateProducts.as_view(), name="createProduct"),
    path('productList/',ProductsList.as_view(), name="productList"),
    path('productDetails/<int:pk>/', ProductsDetail.as_view(), name="productDetails"),

    path('createOrder/', CreateOrder.as_view(), name="createOrder"),
    path('orderList/',OrderList.as_view(), name="orderList"),
    path('orderDetails/<int:pk>/', OrdersDetail.as_view(), name="orderDetails"),

    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
]
urlpatterns = format_suffix_patterns(urlpatterns)